import React, { Component } from 'react'

import { Menu, Button } from 'antd';

import Adduser from '../adduser/Adduser'



class Sidebar extends Component {

  state={
    adduser:false,
    email:""
  }

  handleclick=()=>{
    this.setState({
      adduser:!this.state.adduser
    })
  }

    render() {  

    return (
      
      <div style={{ width: 200 }}>
        {this.state.adduser ? <Adduser/> : ""}
        <Menu
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          mode="inline"
          theme="white"
         
        >
          <Menu.Item key="1">
            <span>
              
              
            </span>
          </Menu.Item>
          <Menu.Item key="2">
            
            <span><Button onClick={this.handleclick}>Add User</Button></span>
          </Menu.Item>
          <Menu.Item key="3">
            
      
          </Menu.Item>
        </Menu>
      </div>
    )
  }
}


export default Sidebar
