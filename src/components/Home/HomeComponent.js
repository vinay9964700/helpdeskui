import React, { Component } from 'react'
import LoginService from './HomeService'


import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';


import Style from './style'

function TabContainer({ children, dir }) {
    return (
      <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
        {children}
      </Typography>
    );
  }
  




class HomeComponent extends Component {
    
    state = {
        value: 0,
        email:"",
        password:"",
        open: false,
        otp:"",
        verified:false,
        notverified:false,
        baropen:false,
        passworddig:false,

      };
    
      handleChange = (event, value) => {
        this.setState({ value });
      };
    
      handleChangeIndex = index => {
        this.setState({ value: index });
      };


      inputChange=value=>(e)=>{
          this.setState({
            [value]:e.target.value
          })

      }

      loginSubmit=(e)=>{
        let data={
            email:this.state.email,
            password:this.state.password
        }
        LoginService.login(data,(err,res)=>{
            if(!err){
                if(res.result[0].role==="user"){
                    this.props.history.push({
                        pathname: '/UserDashboard',
                        state: { detail: this.state.email }
                      })
                }else{
                    this.props.history.push({
                        pathname: "/Admindashboard",
                        state: { detail: this.state.email }
                    })
                }
                
            }
            else{
                alert("wrong password")
            }             
         }                      
        )
      }

      handleClickOpen = () => {
        this.setState({ open: true });
      };
    
      handleClose = () => {
        this.setState({ open: false });
      };

      sendmail=(e)=>{
          let data={
              email:this.state.email
          }
          
          LoginService.sendMail(data,(err,res)=>{   
             if(res.data.status===200){
                   this.setState({
                      open:true
                  }) 
               
              }
              else{
                  alert("NO such User Present")
              }  
              
          })
      }

      handelotp=(e)=>{
       
        let obj={
            email:this.state.email,
            otp:this.state.otp
        }
        LoginService.verify_email(obj,(err,data)=>{
            if(!err){
                this.setState({
                    verified:true,
                    passworddig:true
                })
                alert("OTP verified Kindly Update the password")
            }else{
                console.log(err)
                alert("INCORRECT OTP")
                window.location.reload(); 
                /* this.setState({
                    notverified:true
                }) */
            }
        })
      }

      updatepass=(e)=>{
          let obj={
              email:this.state.email,
              password:this.state.password
          }
        LoginService.savepassword(obj,(err,res)=>{
            
            if(!err){
                this.setState({
                    passworddig:false       
                })
                alert("Password Updated Successfully Kindly Login")
                window.location.reload();                               
            }
            else{
                alert("Password Updation Failed")
            }
        })
      }

      passclose=(e)=>{
          this.setState({
              passworddig:false
          })
      }


    

    render() {
    const { classes } = this.props;
    let dial=this.state.open
    let verified=this.state.verified



    
    const passdialog=(
        <div>
            <Dialog
            open={this.state.passworddig}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle id="form-dialog-title"></DialogTitle>
            <DialogContent>
            <TextField
                autoFocus
                margin="dense"
                id="name"
                label="Enter the password"
                type="email"
                fullWidth
                onChange={this.inputChange("password")}
            />
            </DialogContent>
            <DialogActions>
            <Button   onClick={this.updatepass} color="primary">
                Submit
            </Button>
            <Button  color="primary" onClick={this.passclose}>
                Cancel
            </Button>
            </DialogActions>
        </Dialog>
      </div>
    )

        return (

            <div>
                {passdialog}
                {dial ? 
                <Dialog
                open={this.state.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
              >
                <DialogTitle id="form-dialog-title"></DialogTitle>
                <DialogContent>
                  <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label="Enter OTP sent to mail"
                    type="email"
                    fullWidth
                    onChange={this.inputChange("otp")}
                  />
                </DialogContent>
                <DialogActions>
                  <Button   onClick={this.handelotp} color="primary">
                    Submit
                  </Button>
                  <Button  color="primary" onClick={this.handleClose}>
                    Cancel
                  </Button>
                </DialogActions>
              </Dialog> : null }
            
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Paper className={classes.paper} elevation={1}>
                        <AppBar position="static" color="default">
                            <Tabs
                                value={this.state.value}
                                onChange={this.handleChange}
                                indicatorColor="primary"
                                textColor="primary"
                                variant="fullWidth"
                            >
                                <Tab label="LOGIN" />
                                <Tab label="SET PASSWORD" />
                            
                            </Tabs>
                        </AppBar>
                        <SwipeableViews
                            
                            index={this.state.value}
                            onChangeIndex={this.handleChangeIndex}
                        >
                            <TabContainer >
                                <TextField
                                    id="standard-search"
                                    label="Email id"
                                    type="search"
                                    className={classes.textField}
                                    margin="normal"
                                    onChange={this.inputChange("email")}
                                />
                                
                                <br/>
                                <TextField
                                    id="standard-password-input"
                                    label="Password"
                                    className={classes.textField}
                                    type="password"
                                    autoComplete="current-password"
                                    margin="normal"
                                    onChange={this.inputChange("password")}
                                />
                                <br/>
                                <Button 
                                    variant="contained" 
                                    color="primary" 
                                    className={classes.button}
                                    onClick={this.loginSubmit}>
                                    LOGIN
                                </Button>
                            </TabContainer>
                            <TabContainer >
                                <TextField
                                        id="standard-search"
                                        label="Enter your mail id"
                                        type="search"
                                        className={classes.textField}
                                        margin="normal"
                                        onChange={this.inputChange("email")}
                                    /> 
                                    <br/>
                                <Button 
                                    variant="contained" 
                                    color="primary" 
                                    
                                    onClick={this.sendmail}
                                    className={classes.button}>
                                    SUBMIT
                                </Button> 
                                 
                            </TabContainer>
                            
                            
                       </SwipeableViews> 
                    </Paper>
                </Grid>
            </div>
        )
    }
}



export default withStyles(Style)(HomeComponent)
