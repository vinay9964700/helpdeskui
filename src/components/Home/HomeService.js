import React, { Component } from 'react'
import axios from 'axios';
import URL from '../../api';




export default class HomeService extends Component {
  
    static login(obj,cb){
        axios.post(URL.login,obj)
         .then(res=>{

             if(res.data.status===200){
              
                cb(false,res.data)                      
             }else{
                 cb(true,res)
             }
             
        })
        .catch(rej=>{
            cb(true,rej)
        }) 
    }
    
    static sendMail(obj,cb){
        axios.post(URL.GENERATE_PASSWORD,obj)
        .then(res=>{
            cb(false,res)
        })
        .catch(rej=>{
            console.log(rej)
        })
    }

    static verify_email(obj,cb){
        axios.post(URL.VERIFY_OTP,obj)
        .then(res=>{
            if(res.data.status===200){
                cb(false,res)
            }else{
                cb(true,res)
            }           
        })
        .catch(rej=>{cb(true,rej)})
    }

    static savepassword(obj,cb){
        axios.post(URL.SAVE_PASSWORD,obj)
        .then(res=>{
            cb(false,res)
        })
        .catch(rej=>{
            cb(true,rej)
        })
    }

}
