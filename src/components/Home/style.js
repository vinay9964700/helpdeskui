import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const styles = theme =>({
    paper: {
        padding:20,
        marginTop:150,
        minHeight:400,
        minWidth:600

    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 250,
      },
      button: {
        margin: theme.spacing.unit,
      }
})

export default styles