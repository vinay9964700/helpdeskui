
import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';


//allrequest from
import Allreq from '../Allrequest'

//side bar from ccommon component
import Side from '../common/Sidebar'


import styles from './style';







class Admindashboard extends Component {

  constructor(props){
    super(props)
    this.state={
      adduser:false
    }
  }
  

  render() {


    console.log(this.props)
    const { classes } = this.props;

    return (
      <div className={classes.root}>
      <Grid container spacing={24}>
          <AppBar position="static">
            <Toolbar/>
          </AppBar>
      </Grid>
      <Grid container spacing={24}>
        <Grid item xs={2} position="static">
          <Side/>
        </Grid>
        <Grid item xs={10}>
          <Paper className={classes.paper}><Allreq/></Paper>
        </Grid>
      </Grid>
    </div>
    )
  }
}


export default withStyles(styles)(Admindashboard);