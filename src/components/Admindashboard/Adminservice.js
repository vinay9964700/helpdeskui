import React, { Component } from 'react'
import axios from 'axios'
import URL from '../../api'

export default class Adminservice extends Component {
  

    static accpet_request(obj,cb){
        console.log(obj)
        axios.post(URL.ACCEPT_REQUEST,obj)
        .then(res=>{
            if(res.status===200){
                cb(false,res)
            }else{
                cb(true,res)
            }
        }).catch(rej=>{
            cb(true,rej)
        })
    }
}
