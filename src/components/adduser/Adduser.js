import React, { Component } from 'react'
import axios from 'axios'

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import URL from '../../api'

class Adduser extends Component {
    
    state={
        email:"",
        open:true
    }

    handleChange=(e)=>{
        this.setState({
            email:e.target.value
        })
    }

    handleclose=()=>{
        this.setState({
            open:false
        })
    }

    submit=(e)=>{
        const data={
            email:this.state.email
        }
        axios.post(URL.SAVE_USER,data)
        .then(res=>{
            alert("User Added Successfully")
            this.setState({
                open:false
            })
        })
        .catch(rej=>{
            alert("ERROR in Adding User");
            this.setState({
                open:false
            })
        })
    }



  render() {
      
    return (
      <div>
        <div>
        <Dialog
          open={this.state.open}         
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Add User</DialogTitle>
          <DialogContent>
            
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Email Address"
              type="email"
              fullWidth
              onChange={this.handleChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleclose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.submit} color="primary">
              Add
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      </div>
    )
  }
}


export default Adduser
