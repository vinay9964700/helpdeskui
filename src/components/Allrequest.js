import React, { Component } from 'react'
import { Table } from 'antd';
import './app.css'
import axios from 'axios'
import Service from './Admindashboard/Adminservice'


import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Slide from '@material-ui/core/Slide';
import { Card } from 'antd';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { Alert } from 'antd';






const styles = {
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}


class Allrequest extends Component {

  constructor(props){
    super(props);
    this.state={
      row:[],
      selectedrow:[],
      digopen:false,
      loggedinuser:"",
      assigneduser:"",
      Reqtype:"",
      snack:false
    }
  }
  

  componentDidMount(){
    axios.get('/admin/dashboard')
    .then(res=>{
      
      this.setState({
        row:res.data.result
      })
    
    })
    .catch(rej=>{
      console.log(rej)
    })
  }

  handleClickOpen = () => {
    this.setState({ digopen: true });
  };

  handleClose = () => {
    this.setState({ digopen: false });
  }

  handlechange=value=>(e)=>{
    this.setState({
      [value]:e.target.value
    })
  }

  snackclose=()=>{
    this.setState({
      snack:false
    })
  }

  accept=(e)=>{
    let obj={
      email:this.state.selectedrow.email,
      req_id:this.state.selectedrow.req_id,
      req_type:this.state.Reqtype
    }
    Service.accpet_request(obj,(err,data)=>{
      if(!err){
          this.setState({
            snack:true
          })
          console.log("fromefbjudilb")
      }else{
        alert("error in accepting request")
      }
    })
  }

  render() {
    const { classes } = this.props;
    const digopen=this.state.digopen
    

    const columns = [{
      title: 'Req-Iid',
      dataIndex: 'req_id',
      
    }, {
      title: 'FROM',
      className: 'column-money',
      dataIndex: 'email',
    }, {
      title: 'SENT-DATE',
      dataIndex: 'sent_date',
    }, {
      title: 'SENT-TIME',
      dataIndex: 'sent_time',
    }, {
      title: 'STATUS',
      dataIndex: 'status',
    }, {
      title: 'PRIORITY',
      dataIndex: 'priority',
    }];


    
    
    return (
      <div>
        { digopen ? 
          <Dialog
          fullScreen
          open={this.state.digopen}
          onClose={this.handleClose}
          TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar} color="default">
            <Toolbar>
             <Button color="inherit" onClick={this.handleClose}>
                Close
              </Button>
              <Button variant="outlined" disabled className={classes.button}>FROM : {this.state.selectedrow.email}</Button>
              <Button variant="outlined" disabled className={classes.button}>SENT-DATE : {this.state.selectedrow.sent_date}</Button>
              <Button variant="outlined" disabled className={classes.button}>SENT-TIME : {this.state.selectedrow.sent_time}</Button>
              <Button variant="outlined" disabled className={classes.button}>DEPARTMENT : {this.state.selectedrow.department}</Button>
             
              <Button variant="outlined" disabled className={classes.button}>STATUS : {this.state.selectedrow.status}</Button>
            </Toolbar>
          </AppBar>
          {
              this.state.snack ? <Alert
              message="Request Has been accepted"
              type="success"
              closable
              afterClose={this.snackclose}
            />
           : console.log("false")
             }
          <div style={{ background: '#ECECEC', padding: '30px' }}>
            <Card title="REQUEST DETAILS" bordered={false} style={{ width:1300,height:500 }}>
              <p>{this.state.selectedrow.req_details}</p>
              {console.log(this.state.selectedrow)}
            </Card>
          
            <Button
              variant="contained" 
              color="primary" 
              className={classes.button}
              onClick={this.accept}
              >
              ACCEPT
            </Button>
            <Button variant="contained" color="primary" className={classes.button}>
              CANCEL
            </Button>
           
            <Select             
              onChange={this.handlechange("assigneduser")}
              value={this.state.assigneduser}
            >
              <MenuItem value={"vinay.kumar@neviton.com"}>Vinay Kumar</MenuItem>
              <MenuItem value={"guruprakash.nagaraj@neviton.com"}>Guru Prakash</MenuItem>
            </Select>
            
            <Select             
              onChange={this.handlechange("Reqtype")}
              value={this.state.Reqtype}
              placeholder={"type of request"}
            >
              <MenuItem value={"1d"}>Backup And Restore</MenuItem>
              <MenuItem value={"4h"}>Desktop Software </MenuItem>
              <MenuItem value={"4h"}>Desktop Hardware </MenuItem>
              <MenuItem value={"4h"}>Folder Permission/Cancellation</MenuItem>
              <MenuItem value={"4h"}>Internet /Network</MenuItem>
              <MenuItem value={"4h"}>Mail configuration </MenuItem>
              <MenuItem value={"4h"}>FTP Request</MenuItem>
              <MenuItem value={"1d"}>E-Mail ID Creation or </MenuItem>
              <MenuItem value={"1d"}>New Application Installation </MenuItem>
              <MenuItem value={"1d"}>System Movement</MenuItem>
              <MenuItem value={"1d"}>Virus related problems </MenuItem>
              <MenuItem value={"3d"}>New Hardware</MenuItem>
              <MenuItem value={"2h"}>Password Reset</MenuItem>
              <MenuItem value={"2h"}>Printer Problem</MenuItem>
              <MenuItem value={"2h"}>Server Related Issues</MenuItem>
              <MenuItem value={"3h"}>Others</MenuItem>
              <MenuItem value={"1d"}>Vendor Depedency</MenuItem>
            </Select>
            
          </div>
          
      
        </Dialog> : ""
      
        }
        <Table
          columns={columns}
          dataSource={this.state.row}
          onRowClick={(record, rowIndex)=>{
            this.setState({
              selectedrow:record,
              digopen:true
            })
            
          }}
          bordered
          title={() =>"ALL REQUESTS"}
          
        />
      </div>
    )
  }
}

export default withStyles(styles)(Allrequest)

