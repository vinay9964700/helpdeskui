import React, { Component } from 'react'
import axios from 'axios';
import URL from '../../api';


export default class Dashboardservice extends Component {

    static submit_request(obj,cb){
        axios.post(URL.REQ_SUBMIT,obj)
        .then(res=>{
            if(res.data.status===200){
                cb(false,res.data.result)
            }else{
                cb(true,res.data.message)
            }
        })
        .catch(rej=>{
            cb(true,rej)
        })
    }
    
}
