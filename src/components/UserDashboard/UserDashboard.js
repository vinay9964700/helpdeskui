import React, { Component } from 'react'




import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


import SERVICE from './Dashboardservice';
import styles from './style';




const Department = [
    {
   
      value: 'IT',
    },
    {
 
      value: 'ENGINEERING',
    },
    {
     
      value: 'SUPPORT',
    },
    {
      
      value: 'HR',
    },
  ];

  const Priority = [
    {
      
      value: 'Medium',
    },
    {
      
      value: 'High',
    },
    {
      value: 'Low',
    },
    
  ];

  const Project = [
    {
      value: 'APQP',
    },
    {
      value: 'ENGINEERING',
    },
    {
      value: 'ACCELERO',
    },
    
  ];

  const Dial=()=>(
      <div>
          <p>
              kjenvefuvnbfuebn
          </p>
      </div>
  )


class UserDashboard extends Component {

    constructor(props){
        super(props);
        this.state={
            department:"",
            project:"",
            priority:"",
            multiline:"",
            submit:false,
            req_details:"",
            open: false,
            email:"",

        }
    }

    componentDidMount(props){
        this.setState({
            email:this.props.location.state.detail
        })
    }

    handleClickOpen = () => {
        this.setState({ open: true });
      };
    
      handleClose = () => {
        this.setState({ open: false });
      };

    handleChange = name => event => {
      
        this.setState({
          [name]: event.target.value,
        });
        console.log(this.state.req_details)
        
      };

      handlereq=(e)=>{
          this.props.history.push({
              pathname:"",
              state:{mail:this.state.email}
          })
          this.setState({
              reqopen:!this.state.reqopen
          })
      }

      submit=(e)=>{
          let obj={
              email:this.state.email,
              req_id:this.state.email,
              req_details:this.state.req_details,
              user_id:this.state.u_id,
              priority:this.state.priority,
              project:this.state.project,
              department:this.state.department

          }
          SERVICE.submit_request(obj,(err,data)=>{
              if(!err){
                  alert("Request Submitted Successfully")
              }else{
                alert("Request Submission failed")
              }
          })
      }

      Submitdial=()=>{
          this.setState({
              open:true
          })
      }

      logOut=()=>{
          this.props.history.push("/")
      }


  render() {
    
    const { classes } = this.props
    
      return (
      <div>
        <Paper className={classes.root} elevation={1}>
            <Grid container spacing={24}>
                      <Grid item xs={3}>
                        <TextField
                                id="outlined-select-currency"
                                select
                                label="Select"
                                className={classes.textField}
                                value={this.state.department}
                                onChange={this.handleChange('department')}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                helperText="Department"
                                margin="normal"
                                variant="outlined"
                            >
                                {Department.map(option => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.value}
                                    </MenuItem>
                                ))}
                            </TextField>
                      </Grid>
                      <Grid item xs={3}>
                      <TextField
                                id="outlined-select-currency"
                                select
                                label="Select"
                                value={this.state.priority}
                                className={classes.textField}
                                onChange={this.handleChange('priority')}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                helperText="priority"
                                margin="normal"
                                variant="outlined"
                            >
                                {Priority.map(option => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.value}
                                    </MenuItem>
                                ))}
                            </TextField>
                      </Grid>
                      <Grid item xs={3}>
                      <TextField
                                id="outlined-select-currency"
                                select
                                label="Select"
                                value={this.state.project}
                                className={classes.textField}
                                onChange={this.handleChange('project')}
                                SelectProps={{
                                    MenuProps: {
                                        className: classes.menu,
                                    },
                                }}
                                helperText="Project"
                                margin="normal"
                                variant="outlined"
                            >
                                {Project.map(option => (
                                    <MenuItem value={option.value}>
                                        {option.value}
                                    </MenuItem>
                                ))}
                            </TextField>
                      </Grid>
                      <Grid item xs={3}>
                      
                          <br/>
                          <label htmlFor="contained-button-file">
                              
                          </label>
                      </Grid>
            </Grid>
            <br/>
            <Grid item xs={12}>
                
                <Paper className={classes.control}>
                    <Grid container spacing={24}>
                        <Grid item minWidth="250">
                                  <TextField
                                      id="standard-multiline-flexible"
                                      placeholder="Request Details"
                                      multiline
                                      rowsMax="15"
                                      onChange={this.handleChange('req_details')}
            
                                  />
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
            <div>
                
                <Grid container>
                <Button variant="contained" color="primary" onClick={this.submit} className={classes.button}>
                         Submit
                    </Button>
                    <Button variant="contained" color="primary" onClick={this.handlereq} className={classes.button}>
                         View All Request
                    </Button>
                    <Button onClick={this.logOut} variant="contained" color="primary" className={classes.button}>
                         Log Out
                    </Button>
                    {this.state.open===true ? <Dial/> : null}
                </Grid>
            </div>           
            
        </Paper>
      </div>
    )
  }
}




export default withStyles(styles)(UserDashboard)
