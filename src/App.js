import React from 'react';
import Routes from './Routes';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme=createMuiTheme()

const App = () => (
    <MuiThemeProvider theme={theme}>
      <Routes />
    </MuiThemeProvider>
  );

export default App;