export default {
    login:"/auth/login",
    GENERATE_PASSWORD:"/auth/generate_password",
    VERIFY_OTP:"/auth/verify",
    SAVE_PASSWORD:"/auth/save_password",

    //save the user 
    SAVE_USER:"/admin/adduser",


    //user routes api
    REQ_SUBMIT:"/user/request/submit",



    //admin routes
    ACCEPT_REQUEST:"/admin/request_operations/accept"
}