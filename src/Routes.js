import React from 'react';
import { Route, Switch } from 'react-router-dom';



import HomeComponent from './components/Home/HomeComponent'
import UserDashboard from './components/UserDashboard/UserDashboard'
import Admindashboard from './components/Admindashboard/Admindashboard'
import Allrequest from './components/Allrequest'
import Sidebar from './components/common/Sidebar'





const Routes = () => (
    <Switch>
      
      <Route exact path="/" component={HomeComponent} intial />
      <Route exact path="/UserDashboard" component={UserDashboard} intial />
      <Route exact path="/Admindashboard" component={Admindashboard} intial />
      <Route exact path="/Allrequest" component={Allrequest} intial />
      <Route exact path="/side" component={Sidebar} intial />
   


    </Switch>
  );
  
  export default Routes;